let gameContainer = document.getElementById("game")
const scoreMessageElement = document.getElementById("scoreMessage")

let gifsArray = ["./gifs/1.gif", "./gifs/2.gif", "./gifs/3.gif", "./gifs/4.gif", "./gifs/5.gif", "./gifs/6.gif", "./gifs/7.gif", "./gifs/8.gif", "./gifs/9.gif", "./gifs/10.gif", "./gifs/11.gif", "./gifs/12.gif"]

function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}

// let shuffledColors = shuffle(COLORS);
// let requiredColors = [...shuffledColors, ...shuffledColors]


let totalGifs = [...gifsArray, ...gifsArray]
let shuffledGifs = shuffle(totalGifs)

///////////////////////////////////////////////////////////////////////

function createMainContainer(gif) {

    let mainContainer = document.createElement('div')
    mainContainer.classList.add('main-container')

    let card = document.createElement('div')
    card.classList.add('card')
    mainContainer.appendChild(card)

    let front = document.createElement('div')
    front.classList.add('front')
    front.addEventListener('click', handleCardClick)


    let back = document.createElement('div')
    back.classList.add('back')
    back.style.backgroundImage = `url(${gif})`;
    back.style.backgroundSize = 'cover';
    back.style.backgroundRepeat= 'no-repeat';

    // console.log(back)
    card.appendChild(front)
    card.appendChild(back)

    return mainContainer;
}



function createDivsForColors(shuffledGifs) {

    for (let gif of shuffledGifs) {
        mainContainer = createMainContainer(gif)
        // console.log(mainContainer)
        gameContainer.appendChild(mainContainer)

    }
}

var firstFrontBox, secondFrontBox
var firstBackBox, secondBackBox
let counter = 0;
let correctGuess = 0;



function handleCardClick(event) {

    event.target.parentElement.classList.add('flipping-effect')

    if (!(firstFrontBox)) {
        counter += 1;
        firstFrontBox = event.target
        firstBackBox = firstFrontBox.nextElementSibling
        console.log(firstBackBox)
        // firstBox.removeEventListener("click", handler) no need bc, now back card is on top
        scoreMessageElement.textContent = `Score: ${counter}`
    } else {
        counter += 1;
        gameContainer.style.pointerEvents = "none"
        secondFrontBox = event.target
        secondBackBox = event.target.nextElementSibling
        console.log(secondBackBox)
        // secondBox.removeEventListener("click", handler) no need bc, now back card is on top

        if (secondBackBox.style.backgroundImage !== firstBackBox.style.backgroundImage) {
            // console.log("mismatch")
            setTimeout(() => {
                firstFrontBox.parentElement.classList.remove('flipping-effect')
                secondFrontBox.parentElement.classList.remove('flipping-effect')
                firstBackBox = undefined
                firstFrontBox = undefined
                secondBackBox = undefined
                secondFrontBox = undefined
                gameContainer.style.pointerEvents = "auto"
            }, 1000)
        } else {

            correctGuess+=1;
            gameContainer.style.pointerEvents = "auto"

            console.log("match")

            firstBackBox = undefined
            firstFrontBox = undefined
            secondBackBox = undefined
            secondFrontBox = undefined
        }

        if(correctGuess === gifsArray.length){

            let bestScore = localStorage.getItem('bestScore')

            if (counter<= bestScore){
                localStorage.setItem('bestScore',counter)

                scoreMessageElement.textContent = `Congratulations your score is: ${counter}, you are the best scorer now.`
            scoreMessageElement.classList.add('sucess')
            }else{
                scoreMessageElement.textContent = `Congratulations your score is: ${counter}, Best score is: ${bestScore}`
                scoreMessageElement.classList.add('sucess')
            }

            
        }else{
            scoreMessageElement.textContent = `Score: ${counter}`
        }
    }
}


createDivsForColors(shuffledGifs)
