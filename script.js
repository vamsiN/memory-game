const gameContainer = document.getElementById("game");
const scoreMessageElement = document.getElementById("scoreMessage")

const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple"
];


function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}

let shuffledColors = shuffle(COLORS);

function createMainContainer(color) {

    let mainContainer = document.createElement('div')
    mainContainer.classList.add('main-container')

    let card = document.createElement('div')
    card.classList.add('card')
    mainContainer.appendChild(card)

    let front = document.createElement('div')
    front.classList.add('front')
    front.addEventListener('click', handleCardClick)


    let back = document.createElement('div')
    back.classList.add('back')
    back.style.backgroundColor = color;

    card.appendChild(front)
    card.appendChild(back)

    return mainContainer;
}

function createDivsForColors(shuffledColors) {
    for (let color of shuffledColors) {
        mainContainer = createMainContainer(color)
        // console.log(mainContainer)
        gameContainer.appendChild(mainContainer)

    }

}

var firstFrontBox, secondFrontBox
var firstBackBox, secondBackBox
let counter = 0;

let correctGuess = 0;



function handleCardClick(event) {

    event.target.parentElement.classList.add('flipping-effect')

    if (!(firstFrontBox)) {
        counter += 1;
        firstFrontBox = event.target
        firstBackBox = firstFrontBox.nextElementSibling
        console.log(firstBackBox)
        // firstBox.removeEventListener("click", handler) no need bc, now back card is on top
        scoreMessageElement.textContent = `Score: ${counter}`
    } else {
        counter += 1;
        gameContainer.style.pointerEvents = "none"
        secondFrontBox = event.target
        secondBackBox = event.target.nextElementSibling
        console.log(secondBackBox)
        // secondBox.removeEventListener("click", handler) no need bc, now back card is on top

        if (secondBackBox.style.backgroundColor !== firstBackBox.style.backgroundColor) {
            // console.log("mismatch")
            setTimeout(() => {
                firstFrontBox.parentElement.classList.remove('flipping-effect')
                secondFrontBox.parentElement.classList.remove('flipping-effect')
                firstBackBox = undefined
                firstFrontBox = undefined
                secondBackBox = undefined
                secondFrontBox = undefined
                gameContainer.style.pointerEvents = "auto"
            }, 1000)

            
        } else {

            correctGuess+=1;

            gameContainer.style.pointerEvents = "auto"

            console.log("match")
            
            firstBackBox = undefined
            firstFrontBox = undefined
            secondBackBox = undefined
            secondFrontBox = undefined
        }


        if(correctGuess*2 === COLORS.length){

            let bestScore = localStorage.getItem('bestScore')

            if (counter<= bestScore){
                localStorage.setItem('bestScore',counter)

                scoreMessageElement.textContent = `Congratulations your score is: ${counter}, you are the best scorer now.`
            scoreMessageElement.classList.add('sucess')
            }else{
                scoreMessageElement.textContent = `Congratulations your score is: ${counter}, Best score is: ${bestScore}`
                scoreMessageElement.classList.add('sucess')
            }

            
        }else{
            scoreMessageElement.textContent = `Score: ${counter}`
        }
    }
}

createDivsForColors(shuffledColors);